from django.urls import path
from .views import sayhello, help

urlpatterns = [
    path('', sayhello, name='home'),
    path('help/', help, name='help')
]
