from django.shortcuts import render
from django.http import HttpResponse


def sayhello(request):
    my_dict = {"help": "helloworld/help.html"}
    return render(request, "helloworld/index.html", context=my_dict)

def help(request):
    help_dict = {"help_insert":"HELP PAGE"}
    return render(request, "helloworld/help.html", context=help_dict)

    